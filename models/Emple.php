<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emple".
 *
 * @property int $emp_no
 * @property string $apellido
 * @property string|null $oficio
 * @property int|null $dir
 * @property string|null $fecha_alt
 * @property int|null $salario
 * @property int|null $comision
 * @property int|null $dept_no
 *
 * @property Depart $deptNo
 */
class Emple extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_no', 'apellido','dir'], 'required'],
            [['emp_no', 'dir', 'salario', 'comision', 'dept_no'], 'integer'],
            [['fecha_alt'], 'safe'],
            [['apellido'], 'string', 'max' => 5],
            [['oficio'], 'string', 'max' => 30],
            [['emp_no'], 'unique'],
            [['dept_no'], 'exist', 'skipOnError' => true, 'targetClass' => Depart::className(), 'targetAttribute' => ['dept_no' => 'dept_no'],'message'=>'no existe este departamento'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_no' => 'Numero de empleado',
            'apellido' => 'Apellido',
            'oficio' => 'Oficio',
            'dir' => 'Direccion',
            'fecha_alt' => 'Fecha de Alta',
            'salario' => 'Salario',
            'comision' => 'Comision',
            'dept_no' => 'Dept No',
        ];
    }

    /**
     * Gets query for [[DeptNo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeptNo()
    {
        return $this->hasOne(Depart::className(), ['dept_no' => 'dept_no']);
    }
}
