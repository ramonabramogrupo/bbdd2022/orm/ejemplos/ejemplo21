<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Emple;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="emple-index">
    
     <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $enunciado ?></h1>

        <p class="lead"><?= $sql ?></p>

    </div>
              
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',
        ],
    ]); ?>


</div>
