<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Emple;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Emple', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'deptNo.dnombre',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Emple $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'emp_no' => $model->emp_no]);
                 }
            ],
        ],
    ]); ?>


</div>
