<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Depart;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Departamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dept_no',
            'dnombre',
            'loc',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Depart $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dept_no' => $model->dept_no]);
                 }
            ],
        ],
    ]); ?>


</div>
