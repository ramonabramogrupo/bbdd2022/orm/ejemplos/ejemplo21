<?php

namespace app\controllers;

use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Emple models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'emp_no' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param int $emp_no Codigo de empleado
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($emp_no)
    {
        return $this->render('view', [
            'model' => $this->findModel($emp_no),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Emple();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'emp_no' => $model->emp_no]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $emp_no Codigo de empleado
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($emp_no)
    {
        $model = $this->findModel($emp_no);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'emp_no' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $emp_no Codigo de empleado
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($emp_no)
    {
        $this->findModel($emp_no)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $emp_no Codigo de empleado
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($emp_no)
    {
        if (($model = Emple::findOne(['emp_no' => $emp_no])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionConsulta1()
    {
        // SELECT * FROM emple e WHERE e.dept_no=10;
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find()
                        ->where("dept_no=10"),
        ]);
        
        $enunciado="Todos los empleados del departamento 10";
        $sql="SELECT * FROM emple WHERE dept_no=10";

        return $this->render('consultas', [
            'dataProvider' => $dataProvider,
            'enunciado' => $enunciado,
            'sql' => $sql,
        ]);
    }
    
    public function actionConsulta2()
    {
        /* 
         * Listar los empleados que son vendedores
            SELECT * FROM emple WHERE oficio="vendedor"; 
         */
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find()
                        ->where('oficio="vendedor"'),
        ]);
        
        $enunciado="Listar los empleados que son vendedores";
        $sql='SELECT * FROM emple WHERE oficio="vendedor"';

        return $this->render('consultas', [
            'dataProvider' => $dataProvider,
            'enunciado' => $enunciado,
            'sql' => $sql,
        ]);
    }
    
    public function actionConsulta3()
    {
        /* 
            Listar los empleados que son vendedores 
            y trabajan en el departamento 30 
             SELECT * 
                FROM emple 
                WHERE oficio="vendedor" AND dept_no=30;
        */  
         
        
        $dataProvider=new \yii\data\SqlDataProvider([
                'sql' => 'SELECT * 
                FROM emple 
                WHERE oficio="vendedor" AND dept_no=30'
        ]);
        
        $enunciado="Listar los empleados que son vendedores 
            y trabajan en el departamento 30 ";
        $sql='SELECT * 
                FROM emple 
                WHERE oficio="vendedor" AND dept_no=30';

        return $this->render('consultas', [
            'dataProvider' => $dataProvider,
            'enunciado' => $enunciado,
            'sql' => $sql,
        ]);
    }
    
     public function actionConsulta4()
    {
                
        
        $dataProvider=new \yii\data\SqlDataProvider([
                'sql' => 'SELECT * FROM emple WHERE NOT oficio="vendedor"'
        ]);
        
        $enunciado=" Listar los empleados que no son vendedores";
        $sql='SELECT * FROM emple WHERE NOT oficio="vendedor"';

        return $this->render('consultas', [
            'dataProvider' => $dataProvider,
            'enunciado' => $enunciado,
            'sql' => $sql,
        ]);
    }
    
}
